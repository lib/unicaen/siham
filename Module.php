<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace UnicaenSiham;


use Laminas\Mvc\MvcEvent;
use Laminas\Stdlib\Glob;
use Laminas\Config\Factory as ConfigFactory;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
    }



    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
