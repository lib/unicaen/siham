<?php

namespace UnicaenSiham;


use UnicaenSiham\Controller\Factory\IndexControllerFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenSiham\Controller\IndexController;
use UnicaenSiham\Service\Factory\SihamClientFactory;
use UnicaenSiham\Service\Factory\SihamFactory;
use UnicaenSiham\Service\Siham;
use UnicaenSiham\Service\SihamClient;


return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => 'UnicaenSiham\Controller\Index',
                    'action'     => [
                        'index',
                    ],
                    'roles'      => ['guest'], // pas d'authentification requise
                ],
            ],
        ],
    ],


    'router' => [
        'routes' => [
            'siham' => [
                'type'          => 'Segment',
                'options'       => [
                    'route'    => '/siham',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],

                ],
                'may_terminate' => true,
                'child_routes'  => [],
            ],

        ],
    ],

    'service_manager' => [
        'factories' => [
            SihamClient::class => SihamClientFactory::class,
            Siham::class       => SihamFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

];
