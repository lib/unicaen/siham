# Versions stables

# SIHAM 6.2.0 (09/01/25)

## Nouveautés

* Remplacement de laminas/mail (deprecated) par symfony/mail

# SIHAM 6.1.6 (16/09/24)

## Nouveautés

* Ajout d'une exception quand le webservice PEC ne remonte pas le matricule SIHAM

# SIHAM 6.1.5 (25/06/24)

## Nouveautés

* Prise en compte de la situation matrimoniale lors de la prise en charge de l'intervenant

# SIHAM 6.1.4 (25/03/24)

## Correction

* Correction pour prise en compte du bisTer dans la PEC 

# SIHAM 6.1.3 (16/01/24)

## Corrections

* Auto validation de la clôture du dossier de l'agent


# SIHAM 6.1.2 (02/11/23)

## Corrections

* Coquille dans le nom d'une méthode Siham

# SIHAM 6.1.1 (02/11/23)

## Nouveautés

* Ajout de paramètres de configuration pour la création du contrat lors d'une PEC ou REN

# SIHAM 6.1 (02/11/23)

## Nouveautés

* Ajout des paramètres "contrat" nécessaires pour la PEC et la REN

# SIHAM 6.0.3 (04/05/23)

## Nouveautés

* Ajout d'un nouveau paramètre de configuration pour choisir le type d'affectation remonté

# SIHAM 6.0.1 & 6.0.2

## Corrections

* Suppression de la réduction des codes des structures
* Ajout d'un nouveau paramètre Regex pour permettre de filtrer les libellés des codes structures
* Suppression du code établissement pour la recherche des personnes avec le WS ListeAgents

# Nouveautés

* Passage à Unicaen/Autehentification

# SIHAM 2.6 (04/05/23)

## Nouveautés

* Ajout d'un nouveau paramètre de configuration pour choisir le type d'affectation remonté


# SIHAM 2.4 & 2.5 (28/03/23)

## Corrections

* Suppression de la réduction des codes des structures
* Ajout d'un nouveau paramètre Regex pour permettre de filtrer les libellés des codes structures
* Suppression du code établissement pour la recherche des personnes avec le WS ListeAgents

# SIHAM 2.3 (08/04/22)

## Corrections

* Meilleure gestion des exceptions

# SIHAM 2.2 (05/01/22)

## Nouveautés

* Nouveau paramètre de configuration pour le code permettant de remonter de SIHAM les structures d'affectation

# SIHAM 2.1 (05/01/22)

## Corrections

* Correction au niveau de la méthode sendDebug

# SIHAM 2.0 (05/01/22)

## Nouveautés

* Passage de la librairie sous Laminas

# SIHAM 1.5.3 (05/01/22)

## Nouveautés

* Nouveau paramètre de configuration pour le code permettant de remonter de SIHAM les structures d'affectation

# SIHAM 1.5.2 (03/01/22)

## Corrections

* Correction au niveau de la méthode sendDebug

# SIHAM 1.5.1 (03/01/22)

## Corrections

* Correction au niveau de la méthode sendDebug

# SIHAM 1.5 (07/12/21)

## Nouveautés

* Adaptation de la méthode recupListeAgent suite aux derniers patch SIHAM

# SIHAM 1.4 (28/10/21)

## Nouveautés

* Gestion pour l'inclusion ou l'exclusion des UO dans les listes d'affectation

# SIHAM 1.3 (18/10/21)

## Nouveautés

* Gestion de la situation familiale lors de la prise en charge d'un vacataire

# SIHAM 1.2 (13/10/21)

## Nouveautés

* Ajout de l'auto-validation de la modalité de service lors de la prise en charge ou du renouvellement

# SIHAM 1.0 (27/09/21)

## Nouveautés

* Première version stable de la bibliothèque UnicaenSiham permettant d'intéragir avec les webservices de SIHAM
