# Prise en charge d'un agent

## Introduction

Cette méthode permet la prise en charge d'un agent dans SIHAM. C'est à dire la création d'un agent qui n'existe pas encore dans le SI RH.

Si l'agent que vous souhaitez prendre en charge existe déjà dans SIHAM, il ne faut pas utiliser cette méthode, mais utiliser la méthode renouvellementAgent.

## Méthode et Paramétres

Nom de la méthode : **priseEnChargeAgent($params)**

Paramètre(s) :

**$params** : Array contenant les clés d'entrées suivantes

| Nom de la clé     | Type    | Desc                                                                                  |
|-------------------|---------|---------------------------------------------------------------------------------------|
| categorieEntree         | String  | Matricule de l'agent dont il faut modifier les coordonnées
| civilite            | String  | Email ou numéro de téléphone
| codeAdministration         | Date    | Date de début au format YYY-MM-DD d'utilisation de l'adresse
| codeEtablissement | |
| dateEmbauche | |
| dateNaissance | |
| villeNaissance | |
| departementNaissance | |
| emploi | |
| listeCoordonneesPostales | |
| listeCoordonneesbancaires | |
| listeModalitesServices | |
| listeStatuts | |
| listeNationalites | |
| listeNumerosTelephoneFax | |
| listePositions | |
| listeContrats | |
| motifEntree | |
| nomPatronymique | |
| nomUsuel | |
| numeroInsee | |
| paysNaissance | |
| prenom | |
| sexe | |
| temoinValidite | |
| UO | |

**$type** : Code du type de coordonnées à modifier ou ajouter. Les codes sont récupérables en appelant les constants de la class Siham. Voici la liste des coordonnées modifiables :

Type de coordonnées               | Code numero SIHAM  | Constante de class
|-----------------------------------|--------------------|-----------------------------------
| Téléphone fixe professionnel      | TPR                | Siham::SIHAM_CODE_TYPOLOGIE_FIXE_PRO
| Téléphone fixe personnel          | TPE                | Siham::SIHAM_CODE_TYPOLOGIE_FIXE_PERSO
| Téléphone portable professionnel  | PPR                | Siham::SIHAM_CODE_TYPOLOGIE_PORTABLE_PRO
| Téléphone portable personnel      | PPE                | Siham::SIHAM_CODE_TYPOLOGIE_PORTABLE_PERSO
| Email professionnel               | MPR                | Siham::SIHAM_CODE_TYPOLOGIE_EMAIL_PRO
| Email personnel                   | MPE                | Siham::SIHAM_CODE_TYPOLOGIE_EMAIL_PERSO

Retour :

Retourne True|False selon le succes de la modification. Un exception de type SihamException peut être levée en cas d'erreur de l'API.

