<?php

namespace UnicaenSiham\Service;


use UnicaenSiham\Exception\SihamException;

class SihamClient
{
    const SOAP_VERSION = SOAP_1_1;

    protected $baseUrl;

    protected $wsdl;

    protected $params;

    protected $client;



    public function __construct(string $baseUrl, array $wsdl, array $params = [])
    {
        if ($params !== null) {
            $this->setParams($params);
        }

        if ($wsdl !== null) {
            $this->setWsdl($wsdl);
        }

        if (!empty($baseUrl)) {
            $this->setBaseUrl($baseUrl);
        }
    }



    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }



    public function getParams(): ?array
    {
        return $this->params;
    }



    public function setWsdl(array $wsdl): self
    {
        $this->wsdl = $wsdl;

        return $this;
    }



    public function getWsdl(): ?array
    {
        return $this->wsdl;
    }



    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }



    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }



    public function getClient($webserviceName): \SoapClient
    {


        $params = [
            'login'        => $this->params['login'] ?? null,
            'password'     => $this->params['password'] ?? null,
            'soap_version' => $this->params['version'] ?? self::SOAP_VERSION,
            'cache_wsdl'   => $this->params['cache_wsdl'] ?? 0,
            'trace'        => $this->params['trace'] ?? 0,
            'proxy_host'   => $this->params['proxy_host'] ?? null,
            'proxy_port'   => $this->params['proxy_port'] ?? null,
        ];


        if (array_key_exists($webserviceName, $this->wsdl)) {
            $wsdl = $this->baseUrl . $this->wsdl[$webserviceName];
            try {
                $this->client = new \SoapClient($wsdl, $params);
            } catch (\Exception $e) {
                throw new SihamException($e->getMessage(), 0, $e);
            }
        } else {
            throw new \Exception('Le webservice demandé n\'existe pas');
        }

        return $this->client;
    }



    public function getLastRequest()
    {

        if ($this->client instanceof \SoapClient) {
            return $this->client->__getLastRequest();
        }

        return false;
    }



    public function getLastResponse()
    {

        if ($this->client instanceof \SoapClient) {
            return $this->client->__getLastResponse();
        }

        return false;
    }

}