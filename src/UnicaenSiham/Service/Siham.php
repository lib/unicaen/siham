<?php

namespace UnicaenSiham\Service;


use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use UnicaenSiham\Entity\Agent;
use UnicaenSiham\Exception\SihamException;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\Smtp as SmtpTransport;
use Laminas\Mail\Transport\SmtpOptions;

class Siham
{
    const        SIHAM_TYPE_ACTION_MODIFICATION          = 'M';
    const        SIHAM_TYPE_ACTION_AJOUT                 = 'A';
    const        SIHAM_TYPE_ACTION_SUPPRESSION           = 'S';
    const        SIHAM_TEMOIN_VALIDITE_DEFAULT           = 1;
    const        SIHAM_TEMOIN_ADRESSE_PRINCIPALE         = 1;
    const        SIHAM_MOTIF_ENTREE_DEFAULT              = 'PEC';
    const        SIHAM_CODE_TYPOLOGIE_ADRESSE_PRINCIPALE = "TA01";
    const        SIHAM_CODE_TYPOLOGIE_FIXE_PRO           = "TPR";
    const        SIHAM_CODE_TYPOLOGIE_FIXE_PERSO         = "TPE";
    const        SIHAM_CODE_TYPOLOGIE_PORTABLE_PRO       = "PPR";
    const        SIHAM_CODE_TYPOLOGIE_PORTABLE_PERSO     = "PPE";
    const        SIHAM_CODE_TYPOLOGIE_EMAIL_PRO          = "MPR";
    const        SIHAM_CODE_TYPOLOGIE_EMAIL_PERSO        = "MPE";

    protected $sihamClient;

    protected $sihamConfig;

    protected $codeAdministration;

    protected $codeEtablissement;

    protected $codeNomenclatureStatuts;

    protected $codeNomenclatureGrades;

    protected $codeNomenclatureCorps;

    protected $codeNomenclatureSectionsCnu;

    protected $codeNomenclatureSpecialites;

    protected $codeNomenclatureFamillesProfessionnelles;

    protected $codeNomenclatureQualitesStatutaires;

    protected $codeNomenclatureCategories;

    protected $codeNomenclatureContrats;

    protected $codeNomenclatureModalites;

    protected $codeNomenclaturePositions;

    protected $codeNomenclatureEchelons;

    protected $codeNomenclatureAdministrations;

    protected $codeNomenclatureEtablissements;

    protected $request;

    protected $result;



    public function __construct(SihamClient $sihamClient, array $config)
    {
        $this->sihamClient                              = $sihamClient;
        $this->sihamConfig                              = $config;
        $this->codeEtablissement                        = $config['code-etablissement'];
        $this->codeAdministration                       = $config['code-administration'];
        $this->codeNomenclatureGrades                   = (isset($config['code-nomenclature']['grades'])) ? $config['code-nomenclature']['grades'] : '';
        $this->codeNomenclatureCorps                    = (isset($config['code-nomenclature']['corps'])) ? $config['code-nomenclature']['corps'] : '';
        $this->codeNomenclatureSectionsCnu              = (isset($config['code-nomenclature']['sections-cnu'])) ? $config['code-nomenclature']['section-cnu'] : '';
        $this->codeNomenclatureSpecialites              = (isset($config['code-nomenclature']['specialites'])) ? $config['code-nomenclature']['specialites'] : '';
        $this->codeNomenclatureFamillesProfessionnelles = (isset($config['code-nomenclature']['familles-professionnelles'])) ? $config['code-nomenclature']['familles-professionnelles'] : '';
        $this->codeNomenclatureQualitesStatutaires      = (isset($config['code-nomenclature']['qualites-statutaires'])) ? $config['code-nomenclature']['qualites-statutaires'] : '';
        $this->codeNomenclatureCategories               = (isset($config['code-nomenclature']['categories'])) ? $config['code-nomenclature']['categories'] : '';
        $this->codeNomenclatureContrats                 = (isset($config['code-nomenclature']['type-contrats'])) ? $config['code-nomenclature']['type-contrats'] : '';
        $this->codeNomenclatureStatuts                  = (isset($config['code-nomenclature']['statuts'])) ? $config['code-nomenclature']['statuts'] : '';
        $this->codeNomenclatureModalites                = (isset($config['code-nomenclature']['modalites'])) ? $config['code-nomenclature']['modalites'] : '';
        $this->codeNomenclaturePositions                = (isset($config['code-nomenclature']['positions'])) ? $config['code-nomenclature']['positions'] : '';
        $this->codeNomenclatureEchelons                 = (isset($config['code-nomenclature']['echelons'])) ? $config['code-nomenclature']['echelons'] : '';
        $this->codeNomenclatureAdministrations          = (isset($config['code-nomenclature']['administrations'])) ? $config['code-nomenclature']['administrations'] : '';
        $this->codeNomenclatureEtablissements           = (isset($config['code-nomenclature']['etablissements'])) ? $config['code-nomenclature']['etablissements'] : '';
    }



    public function getClient(): SihamClient
    {
        return $this->sihamClient;
    }



    public function getConfig(): array
    {
        return $this->sihamConfig;
    }



    /**
     * Méthode permettant de rechercher une liste d'agent par nom, prénom etc...
     * Possibilité d'utiliser le joker '?' pour faire des recherches
     *
     * @param array $params Les paramètres possible sont les suivants (au moins l'un doit avoir une valeur) : nomUsuel,
     *                      nomPatronymique, prenom, date de naissance.
     *
     * @throws SihamException
     *
     * @return array
     */

    public function rechercherAgent(array $params): array
    {
        $agents = [];

        $paramsWS = ['ParamRechercheAgent' => [
            'codeEtablissement' => $this->codeEtablissement,
            'nomUsuel'          => (isset($params['nomUsuel'])) ? strtoupper($params['nomUsuel']) : '',
            'nomPatronymique'   => (isset($params['nomPatronymique'])) ? strtoupper($params['nomPatronymique']) : '',
            'prenom'            => (isset($params['prenom'])) ? strtoupper($params['prenom']) : '',
            'dateNaissance'     => (isset($params['dateNaissance'])) ? strtoupper($params['dateNaissance']) : '',
            'codeNIRSsCle'      => (isset($params['codeNIRSsCle'])) ? strtoupper($params['codeNIRSsCle']) : '',
        ],
        ];

        try {
            $client = $this->sihamClient->getClient('RechercheAgentWebService');
            $result = $client->RechercheAgent($paramsWS);
            $this->sendDebug('WS RechercheAgentWebService / METHOD RechercheAgent');

            if (isset($result->return)) {
                if (is_array($result->return)) {
                    foreach ($result->return as $values) {
                        $agent    = new Agent();
                        $agent    = $agent->mapper($values);
                        $agents[] = $agent;
                    }
                } else {
                    $agent    = new Agent();
                    $agent    = $agent->mapper($result->return);
                    $agents[] = $agent;
                }
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS RechercheAgentWebService / METHOD RechercheAgent', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return $agents;
    }



    /**
     * Méthode permettant de récuperer une liste d'agent avec des critères de recherches supplémentaire : nom, prénom, numero INSEE
     * et date d'observation
     *
     * @param array $params Les paramètres possible sont les suivants (au moins l'un doit avoir une valeur) : nomUsuel,
     *                      nomPatronymique, prenom, numeroInsee, dateObservation, temEnseignantChercheur, temEtat
     *
     * @throws SihamException
     *
     * @return array
     */

    public function recupererListeAgents(array $params): array
    {
        $agents = [];

        //Liste type contrat
        $listeTypeContrat = [];

        if (!empty($params['listeTypeContrat'])) {
            foreach ($params['listeTypeContrat'] as $typeContrat) {
                $listeTypeContrat[] = [
                    'codeTypeContrat' => (isset($typeContrat['codeTypeContrat'])) ? $typeContrat['codeTypeContrat'] : '',
                    'modeGest'        => (isset($typeContrat['modeGest'])) ? $typeContrat['modeGest'] : '',
                ];
            }
        } else {
            $listeTypeContrat[] = [
                'codeTypeContrat' => '',
                'modeGest'        => '',
            ];
        }

        $paramsWS = ['ParamRecupListeAgents' => [
            //'codeEtablissement'      => $this->codeEtablissement,
            'nomUsuel'               => (isset($params['nomUsuel'])) ? strtoupper($params['nomUsuel']) : '',
            'nomPatronymique'        => (isset($params['nomPatronymique'])) ? strtoupper($params['nomPatronymique']) : '',
            'prenom'                 => (isset($params['prenom'])) ? strtoupper($params['prenom']) : '',
            'numeroInsee'            => (isset($params['numeroInsee'])) ? $params['numeroInsee'] : '',
            'dateObservation'        => (isset($params['dateObservation'])) ? $params['dateObservation'] : '',
            'listeTypeContrat'       => $listeTypeContrat,
            'temEnseignantChercheur' => (isset($params['TemEnseignantChercheur'])) ? $params['TemEnseignantChercheur'] : '',
            'temEtat'                => (isset($params['TemEtat'])) ? $params['TemEtat'] : '',
            'temoinValide'           => (isset($params['temoinValide'])) ? $params['temoinValide'] : '',

        ]];


        try {
            $client = $this->sihamClient->getClient('ListeAgentsWebService');
            $result = $client->recupListeAgents($paramsWS);
            $this->sendDebug('WS ListeAgentsWebService / METHOD recupListeAgents');

            if (isset($result->return)) {
                if (is_array($result->return)) {
                    foreach ($result->return as $values) {
                        $agent    = new Agent();
                        $agent    = $agent->mapper($values);
                        $agents[] = $agent;
                    }
                } else {
                    $agent    = new Agent();
                    $agent    = $agent->mapper($result->return);
                    $agents[] = $agent;
                }
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS ListeAgentsWebService / METHOD recupListeAgents', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return $agents;
    }



    /**
     *
     * Méthode permettant de récupérer les données personnelles d'un agent (Adresse, téléphone, email etc...)
     *
     * @param $params array Paramètres du webservice : codeEtablissement, dateFinObservation, dateObservation, listeMatricules
     *
     * @return Agent
     */


    public function recupererDonneesPersonnellesAgent(array $params): ?Agent
    {
        $listMatricules = [];
        $agent          = null;
        foreach ($params['listeMatricules'] as $matricule) {
            $listeMatricules[] = ['matricule' => $matricule];
        }


        $paramsWS = ['ParamListAgent' => [
            'codeEtablissement'  => $this->codeEtablissement,
            'dateFinObservation' => (isset($params['dateFinObservation'])) ? $params['dateFinObservation'] : '',
            'dateObservation'    => (isset($params['dateObservation'])) ? $params['dateObservation'] : '',
            'listeMatricules'    => $listeMatricules,
        ]];


        try {
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $result = $client->RecupDonneesPersonnelles($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD RecupDonneesPersonnelles');
            if (isset($result->return)) {
                $agent = new Agent();
                $agent = $agent->mapper($result->return);
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD RecupDonneesPersonnelles', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return $agent;
    }



    /**
     *
     * Méthode permettant de récupérer les données administrative d'un agent (Affectation, carrière, contrat etc...)
     *
     * @param $params array Paramètres du webservice : codeEtablissement, dateFinObservation, dateObservation, listeMatricules
     *
     * @return Agent
     */

    public function recupererDonneesAdministrativeAgent(array $params)
    {
        $listMatricules = [];
        foreach ($params['listeMatricules'] as $matricule) {
            $listeMatricules[] = ['matricule' => $matricule];
        }


        $paramsWS = ['ParamListAgent' => [
            'codeEtablissement'  => $this->codeEtablissement,
            'dateFinObservation' => (isset($params['dateFinObservation'])) ? $params['dateFinObservation'] : '',
            'dateObservation'    => (isset($params['dateObservation'])) ? $params['dateObservation'] : '',
            'listeMatricules'    => $listeMatricules,
        ]];


        try {
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $result = $client->RecupDonneesAdministratives($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD RecupDonneesAdministratives');

            if (isset($result->return)) {

                return $result->return;
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD RecupDonneesAdministratives', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }
    }



    /**
     *
     * Méthode permettant de modifier ou créer l'adresse principale d'un agent dans SIHAM
     *
     * @param $params       array Paramètres du webservice : bisTer, codePostal, complementAdresse,dateDebut, matricule,
     *                      natureVoie, noVoie, nomVoie, ville.
     *
     * @return Boolean
     */

    public function modifierAdressePrincipaleAgent(array $params): bool
    {
        $dateDebut = new \DateTime();

        $paramsWS = ['ParamModifDP' => [
            'codeEtablissement' => (isset($params['codeEtablissement'])) ? strtoupper($params['codeEtablissement']) : '',
            'typeAction'        => (!empty($params['dateDebut'])) ? self::SIHAM_TYPE_ACTION_MODIFICATION : self::SIHAM_TYPE_ACTION_AJOUT,//obligatoire
            'typeAdrPers'       => self::SIHAM_CODE_TYPOLOGIE_ADRESSE_PRINCIPALE,
            'matricule'         => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',//obligatoire
            'bisTer'            => (isset($params['bisTer'])) ? strtoupper($params['bisTer']) : '',
            'natureVoie'        => (isset($params['natureVoie'])) ? strtoupper($params['natureVoie']) : '',
            'noVoie'            => (isset($params['noVoie'])) ? strtoupper($params['noVoie']) : '',
            'nomVoie'           => (isset($params['nomVoie'])) ? strtoupper($params['nomVoie']) : '',
            'codePostal'        => (isset($params['codePostal'])) ? strtoupper($params['codePostal']) : '',
            'complementAdresse' => (isset($params['complementAdresse'])) ? strtoupper(substr($params['complementAdresse'], 0, 37)) : '',
            'ville'             => (isset($params['ville'])) ? strtoupper($params['ville']) : '',
            'codePays'          => (isset($params['codePays'])) ? strtoupper($params['codePays']) : '',
            'dateDebut'         => (!empty($params['dateDebut'])) ? $params['dateDebut'] : $dateDebut->format('Y-m-d'),
            'dateFin'           => '',
        ]];

        try {
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $result = $client->ModifDonneesPersonnelles($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles');

            if (isset($result->return)) {
                if ($result->return->statutMAJ == 1) {
                    return true;
                } else {
                    $message   = $result->return->statutMAJ;
                    $exception = new SihamException($result->return->statutMAJ, 0);
                    $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles', $exception);
                    throw $exception;
                }
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return false;
    }



    /**
     *
     * Méthode permettant d'historiser (soft delete) l'adresse d'un agent dans SIHAM
     *
     * @param $params       array Paramètres du webservice : bisTer, codePostal, complementAdresse,dateDebut, matricule,
     *                      natureVoie, noVoie, nomVoie, ville.
     *
     * @return Boolean
     */

    public function historiserAdresseAgent(array $params, $typeAdresse = 'TA01'): bool
    {
        $dateFin = new \DateTime();

        $paramsWS = ['ParamModifDP' => [
            'bisTer'                 => (isset($params['bisTer'])) ? strtoupper($params['bisTer']) : '',
            'codeEtablissement'      => (isset($params['codeEtablissement'])) ? strtoupper($params['codeEtablissement']) : '',
            'codePays'               => (isset($params['codePays'])) ? strtoupper($params['codePays']) : '',
            'codePostal'             => (isset($params['codePostal'])) ? strtoupper($params['codePostal']) : '',
            'codeUOAffectAdresse'    => (isset($params['codeUOAffectAdresse'])) ? strtoupper($params['codeUOAffectAdresse']) : '',
            'complementAdresse'      => (isset($params['complementAdresse'])) ? strtoupper($params['complementAdresse']) : '',
            'dateDebut'              => (isset($params['dateDebut'])) ? strtoupper($params['dateDebut']) : '',
            'dateFin'                => $dateFin->format('Y-m-d'),//obligatoire,
            'matricule'              => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',//obligatoire
            'natureVoie'             => (isset($params['natureVoie'])) ? strtoupper($params['natureVoie']) : '',
            'noVoie'                 => (isset($params['noVoie'])) ? strtoupper($params['noVoie']) : '',
            'nomVoie'                => (isset($params['nomVoie'])) ? strtoupper($params['nomVoie']) : '',
            'numero'                 => (isset($params['numero'])) ? strtoupper($params['numero']) : '',
            'pourcentageAffectation' => (isset($params['pourcentageAffectation'])) ? strtoupper($params['pourcentageAffectation']) : '',
            'typeAction'             => self::SIHAM_TYPE_ACTION_MODIFICATION,//obligatoire
            'typeAdrPers'            => self::SIHAM_CODE_TYPOLOGIE_ADRESSE_PRINCIPALE,
            'typeNUmero'             => '',
            'ville'                  => (isset($params['ville'])) ? strtoupper($params['ville']) : '',
        ]];

        try {
            //On récupére l'agent pour pouvoir le modifier
            $agent = $this->recupererDonneesPersonnellesAgent(['listeMatricules' => [$params['matricule']]]);


            if ($agent) {
                $paramsWS['ParamModifDP']['dateDebut'] = $agent->getDateDebutAdresse();
                $client                                = $this->sihamClient->getClient('DossierAgentWebService');
                $result                                = $client->ModifDonneesPersonnelles($paramsWS);
                $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles');


                if (isset($result->return)) {
                    if ($result->return->statutMAJ == 1) {
                        return true;
                    } else {
                        $message = $result->return->statutMAJ;
                        throw new SihamException($result->return->statutMAJ, 0);
                    }
                }
            }
        } catch (Exception $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return false;
    }



    /**
     *
     * Méthode permettant de modifier ou créer un téléphone (pro, perso) ou un email (pro, perso) d'un agent dans SIHAM
     *
     * @param $params       array Paramètres du webservice : dateDebut, matricule, numero, typeNumero.
     *
     * @return Boolean
     */

    public function modifierCoordonneesAgent(array $params, $type = null): bool
    {
        if (empty($type)) {
            throw new SihamException("Vous devez préciser le type de coordonnées à mettre à jour : TPR, TPE etc...");
        }

        $dateDebut = new \DateTime();

        $paramsWS = ['ParamModifDP' => [
            'codeEtablissement'   => $this->codeEtablissement,
            'codeUOAffectAdresse' => '',//Obligatoire sinon le WS plante...
            'typeAction'          => (!empty($params['dateDebut'])) ? self::SIHAM_TYPE_ACTION_MODIFICATION : self::SIHAM_TYPE_ACTION_AJOUT,//obligatoire
            'dateDebut'           => (!empty($params['dateDebut'])) ? $params['dateDebut'] : $dateDebut->format('Y-m-d'),
            'dateFin'             => '',//obligatoire,
            'matricule'           => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',//obligatoire
            'numero'              => (isset($params['numero'])) ? $params['numero'] : '',
            'typeNumero'          => $type,
        ]];


        try {
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $result = $client->ModifDonneesPersonnelles($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD modifierCoordonneesAgent');


            if (isset($result->return)) {
                if ($result->return->statutMAJ == 1) {
                    return true;
                } else {
                    $message = $result->return->statutMAJ;
                    throw new SihamException($result->return->statutMAJ, 0);
                }
            }
        } catch (\SoapFault $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD modifierCoordonneesAgent', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }


        return false;
    }



    /**
     *
     * Méthode permettant d'historiser (soft delete) un téléphone (pro, perso) ou un email (pro, perso) d'un agent dans SIHAM
     *
     * @param $params       array Paramètres du webservice : dateDebut, dateFin, matricule, numero, typeNumero.
     *
     * @return Boolean
     */

    public function historiserCoordonneesAgent(array $params, $type = self::SIHAM_CODE_TYPOLOGIE_FIXE_PRO): bool
    {
        $dateFin = new \DateTime();

        $paramsWS = ['ParamModifDP' => [
            'codeEtablissement' => (isset($params['codeEtablissement'])) ? strtoupper($params['codeEtablissement']) : '',
            'dateFin'           => $dateFin->format('Y-m-d'),//obligatoire,
            'matricule'         => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',//obligatoire
            'typeAction'        => self::SIHAM_TYPE_ACTION_SUPPRESSION,//obligatoire
            'typeNumero'        => $type,
        ]];

        try {
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $agent  = $this->recupererDonneesPersonnellesAgent(['listeMatricules' => [$params['matricule']]]);

            if ($agent) {
                switch ($type) {
                    case Siham::SIHAM_CODE_TYPOLOGIE_FIXE_PRO:
                        $paramsWS['ParamModifDP']['dateDebut'] = $agent->getTelephoneProDateDebut();
                    break;

                    case Siham::SIHAM_CODE_TYPOLOGIE_PORTABLE_PERSO:
                        $paramsWS['ParamModifDP']['dateDebut'] = $agent->getTelephonePersoDateDebut();
                    break;

                    case Siham::SIHAM_CODE_TYPOLOGIE_EMAIL_PERSO:
                        $paramsWS['ParamModifDP']['dateDebut'] = $agent->getEmailPersoDateDebut();
                    break;

                    case Siham::SIHAM_CODE_TYPOLOGIE_EMAIL_PRO:
                        $paramsWS['ParamModifDP']['dateDebut'] = $agent->getEmailProDateDebut();
                    break;
                }
            } else {
                throw new SihamException('Agent non trouvé dans SIHAM', 0);
            }

            $result = $client->ModifDonneesPersonnelles($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles');

            if (isset($result->return)) {
                if ($result->return->statutMAJ == 1) {
                    return true;
                } else {
                    $message = $result->return->statutMAJ;
                    throw new SihamException($result->return->statutMAJ, 0);
                }
            }
        } catch (\Exception $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifDonneesPersonnelles', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return false;
    }



    public function modifierCoordonneesBancairesAgent(array $params): bool
    {
        $paramsWS = ['ParamMCB' => [
            'cleCompte'      => (isset($params['cleCompte'])) ? strtoupper($params['cleCompte']) : '',
            'codeAgence'     => (isset($params['codeAgence'])) ? strtoupper($params['codeAgence']) : '',
            'codeBanque'     => (isset($params['codeBanque'])) ? strtoupper($params['codeBanque']) : '',
            'dateDebBanque'  => (isset($params['dateDebBanque'])) ? strtoupper($params['dateDebBanque']) : '',
            'dateFinBanque'  => (isset($params['dateFinBanque'])) ? strtoupper($params['dateFinBanque']) : '',
            'IBAN'           => (isset($params['IBAN'])) ? strtoupper($params['IBAN']) : '',
            'libelleAgence'  => (isset($params['libelleAgence'])) ? strtoupper($params['libelleAgence']) : '',
            'matricule'      => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',
            'modePaiement'   => (isset($params['modePaiement'])) ? strtoupper($params['modePaiement']) : '',
            'numCompte'      => (isset($params['numCompte'])) ? strtoupper($params['numCompte']) : '',
            'paysBanque'     => (isset($params['paysBanque'])) ? strtoupper($params['paysBanque']) : '',
            'SWIFT'          => (isset($params['SWIFT'])) ? strtoupper($params['SWIFT']) : '',
            'temoinValidite' => (isset($params['temoinValidite'])) ? strtoupper($params['temoinValidite']) : '',
        ]];

        try {
            //On récupére l'agent pour pouvoir le modifier
            $client = $this->sihamClient->getClient('DossierAgentWebService');
            $result = $client->ModifCoordonneesBancaires($paramsWS);
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifCoordonneesBancaires');

            return true;
        } catch (\SoapFault $e) {
            $this->sendDebug('WS DossierAgentWebService / METHOD ModifCoordonneesBancaires', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }
    }



    /**
     *
     * Méthode permettant la prise en charge (création d'un agent) dans SIHAM. Retour le matricule de l'agent SIHAM qui vient d'être créé.
     *
     * @param $params array
     *
     * @return string|null
     */

    public function priseEnChargeAgent($params): ?string
    {
        //Traitement de l'adresse principale
        $listeCoordonneesPostales = [];

        if (!empty($params['listeCoordonneesPostales'])) {
            foreach ($params['listeCoordonneesPostales'] as $coordonnees) {
                $listeCoordonneesPostales[] = [
                    'bureauDistributeur'   => (isset($coordonnees['bureauDistributeur'])) ? strtoupper($coordonnees['bureauDistributeur']) : '',
                    'codePays'             => (isset($coordonnees['codePays'])) ? strtoupper($coordonnees['codePays']) : '',
                    'codePostal'           => (isset($coordonnees['codePostal'])) ? strtoupper($coordonnees['codePostal']) : '',
                    'commune'              => (isset($coordonnees['commune'])) ? strtoupper($coordonnees['commune']) : '',
                    'complementAdresse'    => (isset($coordonnees['complementAdresse'])) ? strtoupper($coordonnees['complementAdresse']) : '',
                    'debutAdresse'         => (isset($coordonnees['debutAdresse'])) ? strtoupper($coordonnees['debutAdresse']) : '',
                    'natureVoie'           => (isset($coordonnees['natureVoie'])) ? strtoupper($coordonnees['natureVoie']) : '',
                    'nomVoie'              => (isset($coordonnees['nomVoie'])) ? strtoupper($coordonnees['nomVoie']) : '',
                    'numAdresse'           => (isset($coordonnees['numAdresse'])) ? strtoupper($coordonnees['numAdresse']) : '',
                    'bisTer'               => (isset($coordonnees['bisTer'])) ? strtoupper($coordonnees['bisTer']) : '',
                    'temAdressePrincipale' => (isset($coordonnees['temAdressePrincipale'])) ? strtoupper($coordonnees['temAdressePrincipale']) : self::SIHAM_TEMOIN_ADRESSE_PRINCIPALE,
                    'temoinValidite'       => (isset($coordonnees['temoinValidite'])) ? strtoupper($coordonnees['temoinValidite']) : self::SIHAM_TEMOIN_VALIDITE_DEFAULT,
                    'typeAdresse'          => (isset($coordonnees['typeAdresse'])) ? strtoupper($coordonnees['typeAdresse']) : self::SIHAM_CODE_TYPOLOGIE_ADRESSE_PRINCIPALE,
                ];
            }
        } else {
            $listeCoordonneesPostales[] = [
                'bureauDistributeur'   => '',
                'codePays'             => '',
                'codePostal'           => '',
                'commune'              => '',
                'debutAdresse'         => '',
                'natureVoie'           => '',
                'nomVoie'              => '',
                'numAdresse'           => '',
                'temAdressePrincipale' => '',
                'temoinValidite'       => '',
                'typeAdresse'          => '',
            ];
        }


        //Traitement des coordonnées bancaires
        $listeCoordonneesBancaires = [];
        if (!empty($params['listeCoordonneesBancaires'])) {
            foreach ($params['listeCoordonneesBancaires'] as $coordonnees) {
                $listeCoordonneesBancaires[] = [
                    'cleCompte'      => (isset($coordonnees['cleCompte'])) ? strtoupper($coordonnees['cleCompte']) : '',
                    'codeAgence'     => (isset($coordonnees['codeAgence'])) ? strtoupper($coordonnees['codeAgence']) : '',
                    'codeBanque'     => (isset($coordonnees['codeBanque'])) ? strtoupper($coordonnees['codeBanque']) : '',
                    'dateDebBanque'  => (isset($coordonnees['dateDebBanque'])) ? strtoupper($coordonnees['dateDebBanque']) : '',
                    'dateFinBanque'  => (isset($coordonnees['dateFinBanque'])) ? strtoupper($coordonnees['dateFinBanque']) : '',
                    'IBAN'           => (isset($coordonnees['IBAN'])) ? strtoupper($coordonnees['IBAN']) : '',
                    'libelleAgence'  => (isset($coordonnees['libelleAgence'])) ? strtoupper($coordonnees['libelleAgence']) : '',
                    'modePaiement'   => (isset($coordonnees['modePaiement'])) ? strtoupper($coordonnees['modePaiement']) : '',
                    'numCompte'      => (isset($coordonnees['numCompte'])) ? strtoupper($coordonnees['numCompte']) : '',
                    'paysBanque'     => (isset($coordonnees['paysBanque'])) ? strtoupper($coordonnees['paysBanque']) : '',
                    'SWIFT'          => (isset($coordonnees['SWIFT'])) ? strtoupper($coordonnees['SWIFT']) : '',
                    'temoinValidite' => (isset($coordonnees['temoinValidite'])) ? strtoupper($coordonnees['temoinValidite']) : '',

                ];
            }
        } else {
            $listeCoordonneesBancaires[] = [
                'cleCompte'      => '',
                'codeAgence'     => '',
                'codeBanque'     => '',
                'dateDebBanque'  => '',
                'dateFinBanque'  => '',
                'IBAN'           => '',
                'libelleAgence'  => '',
                'modePaiement'   => '',
                'numCompte'      => '',
                'paysBanque'     => '',
                'SWIFT'          => '',
                'temoinValidite' => '',

            ];
        }

        //Traitement des modalités de services
        $listeModalitesServices = [];

        if (!empty($params['listeModalitesServices'])) {
            foreach ($params['listeModalitesServices'] as $modalite) {
                $listeModalitesServices[] = ['dateEffetModalite' => $modalite['dateEffetModalite'],
                                             'modalite'          => $modalite['modalite'],
                                             'temoinValidite'    => (isset($modalite['temoinValidite'])) ? $modalite['temoinValidite'] : '',
                ];
            }
        } else {
            $listeModalitesServices[] = ['dateEffetModalite' => '',
                                         'modalite'          => ''];
        }

        //Traitement du statut

        $listeStatuts = [];

        if (!empty($params['listeStatuts'])) {
            foreach ($params['listeStatuts'] as $statut) {
                $listeStatuts[] = ['dateEffetStatut' => $statut['dateEffetStatut'],
                                   'statut'          => $statut['statut']];
            }
        } else {
            $listeStatuts[] = ['dateEffetStatut' => '',
                               'statut'          => ''];
        }

        //Traitement de la nationalité

        $listeNationalites = [];
        if (!empty($params['listeNationalites'])) {
            foreach ($params['listeNationalites'] as $nationalite) {
                $listeNationalites[] = [
                    'nationalite'   => (isset($nationalite['nationalite'])) ? strtoupper($nationalite['nationalite']) : '',
                    'temPrincipale' => '1',

                ];
            }
        } else {
            $listeNationalites[] = [
                'nationalite'   => '',
                'temPrincipale' => '1',
            ];
        }


        //Traitement du numéro de téléphone

        $listeNumerosTelephoneFax = [];
        if (!empty($params['listeNumerosTelephoneFax'])) {
            foreach ($params['listeNumerosTelephoneFax'] as $numero) {
                $listeNumerosTelephoneFax[] = [
                    'dateDebutTel' => (isset($numero['dateDebutTel'])) ? strtoupper($numero['dateDebutTel']) : '',
                    'numero'       => (isset($numero['numero'])) ? $numero['numero'] : '',
                    'typeNumero'   => (isset($numero['typeNumero'])) ? strtoupper($numero['typeNumero']) : '',

                ];
            }
        } else {
            $listeNumerosTelephoneFax[] = [
                'dateDebutTel' => '',
                'numero'       => '',
                'typeNumero'   => '',

            ];
        }

        //Traitement des positions
        $listePositions = [];
        if (!empty($params['listePositions'])) {
            foreach ($params['listePositions'] as $position) {
                $listePositions[] = [
                    'dateEffetPosition' => (isset($position['dateEffetPosition'])) ? strtoupper($position['dateEffetPosition']) : '',
                    'dateFinPrevue'     => (isset($position['dateFinPrevue'])) ? strtoupper($position['dateFinPrevue']) : '',
                    'dateFinReelle'     => (isset($position['dateFinReelle'])) ? strtoupper($position['dateFinReelle']) : '',
                    'position'          => (isset($position['position'])) ? strtoupper($position['position']) : '',
                    'temoinValidite'    => (isset($position['temoinValidite'])) ? strtoupper($position['temoinValidite']) : '',
                ];
            }
        } else {
            $listePositions[] = [
                'dateEffetPosition' => '',
                'position'          => '',
                'temoinValidite'    => '',
            ];
        }

        $listeCarriere = [];
        if (!empty($params['listeCarriere'])) {
            $listeCarriere[] = ['dateEffetCarriere' => (isset($params['listeCarriere']['dateEffetCarriere'])) ? $params['listeCarriere']['dateEffetCarriere'] : '',
                                'grade'             => (isset($params['listeCarriere']['grade'])) ? $params['listeCarriere']['grade'] : '',
                                'qualiteStatutaire' => (isset($params['listeCarriere']['qualiteStatutaire'])) ? $params['listeCarriere']['qualiteStatutaire'] : '',
                                'temoinValidite'    => (isset($params['listeCarriere']['temoinValidite'])) ? $params['listeCarriere']['temoinValidite'] : ''];
        }

        $listeContrats = [];
        if (!empty($params['listeContrats'])) {
            foreach ($params['listeContrats'] as $contrat) {
                $listeContrats[] = [
                    'dateDebutContrat'          => (isset($contrat['dateDebutContrat'])) ? strtoupper($contrat['dateDebutContrat']) : '',
                    'dateFinContrat'            => (isset($contrat['dateFinContrat'])) ? strtoupper($contrat['dateFinContrat']) : '',
                    'natureContrat'             => (isset($contrat['natureContrat'])) ? strtoupper($contrat['natureContrat']) : '',
                    'typeContrat'               => (isset($contrat['typeContrat'])) ? strtoupper($contrat['typeContrat']) : '',
                    'typeLienJuridique'         => (isset($contrat['typeLienJuridique'])) ? strtoupper($contrat['typeLienJuridique']) : '',
                    'modeRemuneration'          => (isset($contrat['modeRemuneration'])) ? strtoupper($contrat['modeRemuneration']) : '',
                    'modeDeGestion'             => (isset($contrat['modeDeGestion'])) ? strtoupper($contrat['modeDeGestion']) : '',
                    'categorieContrat'          => (isset($contrat['categorieContrat'])) ? strtoupper($contrat['categorieContrat']) : '',
                    'chevron'                   => (isset($contrat['chevron'])) ? strtoupper($contrat['chevron']) : '',
                    'commentaireContrat'        => (isset($contrat['commentaireContrat'])) ? strtoupper($contrat['commentaireContrat']) : '',
                    'dateFinPrevue'             => (isset($contrat['dateFinPrevue'])) ? strtoupper($contrat['dateFinPrevue']) : '',
                    'debutPeriodeEssai'         => (isset($contrat['debutPeriodeEssai'])) ? strtoupper($contrat['debutPeriodeEssai']) : '',
                    'deviseRemunerationContrat' => (isset($contrat['deviseRemunerationContrat'])) ? strtoupper($contrat['deviseRemunerationContrat']) : '',
                    'finPeriodeEssai'           => (isset($contrat['finPeriodeEssai'])) ? strtoupper($contrat['finPeriodeEssai']) : '',
                    'gradeTG'                   => (isset($contrat['gradeTG'])) ? strtoupper($contrat['gradeTG']) : '',
                    'horsEchelleLettre'         => (isset($contrat['horsEchelleLettre'])) ? strtoupper($contrat['horsEchelleLettre']) : '',
                    'indiceRemu'                => (isset($contrat['indiceRemu'])) ? strtoupper($contrat['indiceRemu']) : '',
                    'nbHeuresContrat'           => (isset($contrat['nbHeuresContrat'])) ? strtoupper($contrat['nbHeuresContrat']) : '',
                    'pourcentage'               => (isset($contrat['pourcentage'])) ? strtoupper($contrat['pourcentage']) : '',
                    'remunerationContrat'       => (isset($contrat['remunerationContrat'])) ? strtoupper($contrat['remunerationContrat']) : '',
                    'tauxHoraires'              => (isset($contrat['tauxHoraires'])) ? strtoupper($contrat['tauxHoraires']) : '',
                    'temoinValidite'            => (isset($contrat['temoinValidite'])) ? strtoupper($contrat['temoinValidite']) : '',

                ];
            }
        }

        $listeSituations = [];
        if (!empty($params['listeSituations'])) {
            foreach ($params['listeSituations'] as $situation) {
                $listeSituations[] = [
                    'dateEffetSituFam' => (isset($situation['dateEffetSituFam'])) ? $situation['dateEffetSituFam'] : '',
                    'situFam'          => (isset($situation['situFam'])) ? $situation['situFam'] : '',
                    'temoinValidite'   => (isset($situation['temoinValidite'])) ? $situation['temoinValidite'] : '',
                ];
            }
        }

        $paramsWS = ['ParamPEC' => [
            'categorieEntree'           => (isset($params['categorieEntree'])) ? strtoupper($params['categorieEntree']) : '',
            'civilite'                  => (isset($params['civilite'])) ? strtoupper($params['civilite']) : '',
            'codeAdministration'        => $this->codeAdministration,
            'codeEtablissement'         => $this->codeEtablissement,
            'dateEmbauche'              => (isset($params['dateEmbauche'])) ? strtoupper($params['dateEmbauche']) : '',
            'dateNaissance'             => (isset($params['dateNaissance'])) ? strtoupper($params['dateNaissance']) : '',
            'villeNaissance'            => (isset($params['villeNaissance'])) ? strtoupper($params['villeNaissance']) : '',
            'departementNaissance'      => (isset($params['departementNaissance'])) ? strtoupper($params['departementNaissance']) : '',
            'paysNaissance'             => (isset($params['paysNaissance'])) ? strtoupper($params['paysNaissance']) : '',
            'emploi'                    => (isset($params['emploi'])) ? strtoupper($params['emploi']) : '',
            'listeCoordonneesPostales'  => $listeCoordonneesPostales,
            'listeCoordonneesbancaires' => $listeCoordonneesBancaires,
            'listeModalitesServices'    => $listeModalitesServices,
            'listeStatuts'              => $listeStatuts,
            'listeNationalites'         => $listeNationalites,
            'listeNumerosTelephoneFax'  => $listeNumerosTelephoneFax,
            'listePositions'            => $listePositions,
            'listeCarriere'             => $listeCarriere,
            'listeContrats'             => $listeContrats,
            'listesituationsfamiliales' => $listeSituations,
            'motifEntree'               => (isset($params['motifEntree'])) ? strtoupper($params['motifEntree']) : self::SIHAM_MOTIF_ENTREE_DEFAULT,
            'nomPatronymique'           => (isset($params['nomPatronymique'])) ? strtoupper($params['nomPatronymique']) : '',
            'nomUsuel'                  => (isset($params['nomUsuel'])) ? strtoupper($params['nomUsuel']) : '',
            'numeroInsee'               => (isset($params['numeroInsee'])) ? strtoupper($params['numeroInsee']) : '',
            'numeroInseeProvisoire'     => (isset($params['numeroInseeProvisoire'])) ? strtoupper($params['numeroInseeProvisoire']) : '',
            'prenom'                    => (isset($params['prenom'])) ? strtoupper($params['prenom']) : '',
            'sexe'                      => (isset($params['sexe'])) ? strtoupper($params['sexe']) : '1',
            'temoinValidite'            => (isset($params['temoinValidite'])) ? strtoupper($params['temoinValidite']) : '',
            'UO'                        => (isset($params['UO'])) ? strtoupper($params['UO']) : '',

        ]];

        try {
            $client = $this->sihamClient->getClient('PECWebService');
            $result = $client->PriseEnChargeAgent($paramsWS);
            $this->sendDebug('WS PECWebService / METHOD PriseEnChargeAgent');
        } catch (\Exception $e) {
            $this->sendDebug('WS PECWebService / METHOD PriseEnChargeAgent', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        if ($result->return->statut == 'ERREUR_GENERALE') {
            $messageErreur  = '';
            $messageWarning = '';
            if (!empty($result->return->listeAnomaliesWebServices)) {
                if (is_array($result->return->listeAnomaliesWebServices)) {
                    foreach ($result->return->listeAnomaliesWebServices as $anomalie) {
                        if (isset($anomalie->anomalie)) {
                            if (strpos($anomalie->anomalie, 'Erreur') === 0) {
                                $messageErreur .= $anomalie->anomalie . '<br/>';
                            }
                        }
                    }
                } else {
                    if (isset($result->return->listeAnomaliesWebServices->anomalie)) {
                        if (strpos($result->return->listeAnomaliesWebServices->anomalie, 'Erreur') === 0) {
                            $messageErreur .= $result->return->listeAnomaliesWebServices->anomalie . '<br/>';
                        }
                    }
                }
            }
            //Traitement du message d'erreur spécifique à la PEC
            $exception = new SihamException($messageErreur, 0);
            $this->sendDebug('WS PECWebService / METHOD PriseEnChargeAgent', $exception);
            throw $exception;
        } elseif ($result->return->statut == 'MAJ OK') {
            if(!empty($result->return->matricule))
            {
                return $result->return->matricule;
            }
            else{
                $exception = new SihamException('Matricule de l\'agent non généré lors de la prise en charge', 0);
                $this->sendDebug('WS PECWebService / METHOD PriseEnChargeAgent', $exception);
                throw $exception;
            }
        }
        else {
            $exception = new SihamException('Erreur non identifiée, veuillez vous rapprocher du support informatique', 0);
            $this->sendDebug('WS PECWebService / METHOD PriseEnChargeAgent', $exception);
            throw $exception;
        }


        return null;
    }



    /**
     *
     * Méthode permettant le renouvellement d'un agent existant dans SIHAM. Retour le matricule de l'agent SIHAM qui vient d'être créé.
     *
     * @param $params array
     *
     * @return string|null
     */

    public function renouvellementAgent(array $params): ?string
    {

        //Traitement des modalités de services
        $listeModalitesServices = [];

        if (!empty($params['listeModalitesServices'])) {
            foreach ($params['listeModalitesServices'] as $modalite) {
                $listeModalitesServices[] = ['dateEffetModalite' => $modalite['dateEffetModalite'],
                                             'modalite'          => $modalite['modalite'],
                                             'temoinValidite'    => (isset($modalite['temoinValidite'])) ? $modalite['temoinValidite'] : ''];
            }
        } else {
            $listeModalitesServices[] = ['dateEffetModalite' => '',
                                         'modalite'          => ''];
        }

        //Traitement du statut
        $listeStatuts = [];

        if (!empty($params['listeStatuts'])) {
            foreach ($params['listeStatuts'] as $statut) {
                $listeStatuts[] = ['dateEffetStatut' => $statut['dateEffetStatut'],
                                   'statut'          => $statut['statut']];
            }
        } else {
            $listeStatuts[] = ['dateEffetStatut' => '',
                               'statut'          => ''];
        }

        $listeCarriere = [];
        if (!empty($params['listeCarriere'])) {
            $listeCarriere[] = ['dateEffetCarriere' => (isset($params['listeCarriere']['dateEffetCarriere'])) ? $params['listeCarriere']['dateEffetCarriere'] : '',
                                'grade'             => (isset($params['listeCarriere']['grade'])) ? $params['listeCarriere']['grade'] : '',
                                'qualiteStatutaire' => (isset($params['listeCarriere']['qualiteStatutaire'])) ? $params['listeCarriere']['qualiteStatutaire'] : '',
                                'temoinValidite'    => (isset($params['listeCarriere']['temoinValidite'])) ? $params['listeCarriere']['temoinValidite'] : ''];
        }

        //Traitement des positions
        $listePositions = [];


        if (!empty($params['listePositions'])) {
            foreach ($params['listePositions'] as $position) {
                $listePositions[] = [
                    'dateEffetPosition' => (isset($position['dateEffetPosition'])) ? strtoupper($position['dateEffetPosition']) : '',
                    'dateFinPrevue'     => (isset($position['dateFinPrevue'])) ? strtoupper($position['dateFinPrevue']) : '',
                    'dateFinReelle'     => (isset($position['dateFinReelle'])) ? strtoupper($position['dateFinReelle']) : '',
                    'position'          => (isset($position['position'])) ? strtoupper($position['position']) : '',
                    'temoinValidite'    => (isset($position['temoinValidite'])) ? strtoupper($position['temoinValidite']) : '',
                ];
            }
        } else {
            $listePositions[] = [
                'dateEffetPosition' => '',
                'position'          => '',
                'temoinValidite'    => '',
            ];
        }

        //Traitement du contrat
        $listeContrats = [];
        if (!empty($params['listeContrats'])) {
            foreach ($params['listeContrats'] as $contrat) {
                $listeContrats[] = [
                    'dateDebutContrat'          => (isset($contrat['dateDebutContrat'])) ? strtoupper($contrat['dateDebutContrat']) : '',
                    'dateFinContrat'            => (isset($contrat['dateFinContrat'])) ? strtoupper($contrat['dateFinContrat']) : '',
                    'natureContrat'             => (isset($contrat['natureContrat'])) ? strtoupper($contrat['natureContrat']) : '',
                    'typeContrat'               => (isset($contrat['typeContrat'])) ? strtoupper($contrat['typeContrat']) : '',
                    'typeLienJuridique'         => (isset($contrat['typeLienJuridique'])) ? strtoupper($contrat['typeLienJuridique']) : '',
                    'modeRemuneration'          => (isset($contrat['modeRemuneration'])) ? strtoupper($contrat['modeRemuneration']) : '',
                    'modeDeGestion'             => (isset($contrat['modeDeGestion'])) ? strtoupper($contrat['modeDeGestion']) : '',
                    'categorieContrat'          => (isset($contrat['categorieContrat'])) ? strtoupper($contrat['categorieContrat']) : '',
                    'chevron'                   => (isset($contrat['chevron'])) ? strtoupper($contrat['chevron']) : '',
                    'commentaireContrat'        => (isset($contrat['commentaireContrat'])) ? strtoupper($contrat['commentaireContrat']) : '',
                    'dateFinPrevue'             => (isset($contrat['dateFinPrevue'])) ? strtoupper($contrat['dateFinPrevue']) : '',
                    'debutPeriodeEssai'         => (isset($contrat['debutPeriodeEssai'])) ? strtoupper($contrat['debutPeriodeEssai']) : '',
                    'deviseRemunerationContrat' => (isset($contrat['deviseRemunerationContrat'])) ? strtoupper($contrat['deviseRemunerationContrat']) : '',
                    'finPeriodeEssai'           => (isset($contrat['finPeriodeEssai'])) ? strtoupper($contrat['finPeriodeEssai']) : '',
                    'gradeTG'                   => (isset($contrat['gradeTG'])) ? strtoupper($contrat['gradeTG']) : '',
                    'horsEchelleLettre'         => (isset($contrat['horsEchelleLettre'])) ? strtoupper($contrat['horsEchelleLettre']) : '',
                    'indiceRemu'                => (isset($contrat['indiceRemu'])) ? strtoupper($contrat['indiceRemu']) : '',
                    'nbHeuresContrat'           => (isset($contrat['nbHeuresContrat'])) ? strtoupper($contrat['nbHeuresContrat']) : '',
                    'pourcentage'               => (isset($contrat['pourcentage'])) ? strtoupper($contrat['pourcentage']) : '',
                    'remunerationContrat'       => (isset($contrat['remunerationContrat'])) ? strtoupper($contrat['remunerationContrat']) : '',
                    'tauxHoraires'              => (isset($contrat['tauxHoraires'])) ? strtoupper($contrat['tauxHoraires']) : '',
                    'temoinValidite'            => (isset($contrat['temoinValidite'])) ? strtoupper($contrat['temoinValidite']) : '',

                ];
            }
        }



        $paramsWS = ['ParamRA' => [
            'categorieEntree'        => (isset($params['categorieEntree'])) ? strtoupper($params['categorieEntree']) : '',
            'codeAdministration'     => $this->codeAdministration,
            'codeEtablissement'      => $this->codeEtablissement,
            'dateRenouvellement'     => (isset($params['dateRenouvellement'])) ? strtoupper($params['dateRenouvellement']) : '',
            'emploi'                 => (isset($params['emploi'])) ? strtoupper($params['emploi']) : '',
            'listeCarriere'          => (!empty($listeCarriere)) ? $listeCarriere : '',
            'listeModalitesServices' => $listeModalitesServices,
            'listeStatuts'           => $listeStatuts,
            'listePositions'         => $listePositions,
            'listeContrats'          => $listeContrats,
            //'motifEntree'            => (isset($params['motifEntree'])) ? strtoupper($params['motifEntree']) : self::SIHAM_MOTIF_ENTREE_DEFAULT,
            'matricule'              => (isset($params['matricule'])) ? strtoupper($params['matricule']) : '',
            'temoinValidite'         => (isset($params['temoinValidite'])) ? strtoupper($params['temoinValidite']) : '',
            'UO'                     => (isset($params['UO'])) ? strtoupper($params['UO']) : '',

        ]];

        try {
            $client = $this->sihamClient->getClient('PECWebService');
            $result = $client->RenouvellementAgent($paramsWS);
            $this->sendDebug('WS PECWebService / METHOD RenouvellementAgent');
        } catch (\Exception $e) {
            $this->sendDebug('WS PECWebService / METHOD RenouvellementAgent', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }
        if ($result->return->statut == 'ERREUR_GENERALE') {
            $messageErreur  = '';
            $messageWarning = '';
            if (!empty($result->return->listeAnomaliesWebServices)) {
                if (is_array($result->return->listeAnomaliesWebServices)) {
                    foreach ($result->return->listeAnomaliesWebServices as $anomalie) {
                        if (isset($anomalie->anomalie)) {
                            if (strpos($anomalie->anomalie, 'Erreur') === 0) {
                                $messageErreur .= $anomalie->anomalie . '<br/>';
                            }
                        }
                    }
                } else {
                    if (isset($result->return->listeAnomaliesWebServices->anomalie)) {
                        if (strpos($result->return->listeAnomaliesWebServices->anomalie, 'Erreur') === 0) {
                            $messageErreur .= $result->return->listeAnomaliesWebServices->anomalie . '<br/>';
                        }
                    }
                }
            }

            $exception = new SihamException($messageErreur, 0);
            $this->sendDebug('WS PECWebService / METHOD RenouvellementAgent', $exception);
            throw $exception;
        } elseif ($result->return->statut == 'MAJ OK' && !empty($result->return->matricule)) {
            return $result->return->matricule;
        } else {
            $exception = new SihamException('Erreur non identifié, veuillez vous rapprocher du support informatique', 0);
            $this->sendDebug('WS PECWebService / METHOD RenouvellementAgent', $exception);
            throw $exception;
        }

        return null;
    }



    public function cloreDossier(array $params): bool
    {

        $paramsWS = ['ParamCLD' => [
            'categorieSituation' => (isset($params['categorieSituation'])) ? $params['categorieSituation'] : '',
            'dateSortie'         => (isset($params['dateSortie'])) ? $params['dateSortie'] : '',
            'matricule'          => (isset($params['matricule'])) ? $params['matricule'] : '',
            'motifSituation'     => (isset($params['motifSituation'])) ? $params['motifSituation'] : '',
            'temoinValidite'     => (isset($params['temoinValidite'])) ? $params['temoinValidite'] : '',
        ]];

        try {

            $client = $this->sihamClient->getClient('PECWebService');
            $result = $client->CloreLeDossier($paramsWS);
            $this->sendDebug('WS PECWebService / METHOD CloreLeDossier');
        } catch (\Exception $e) {
            $this->sendDebug('WS PECWebService / METHOD CloreLeDossier', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }

        return true;
    }



    /**
     *
     * Méthode permettant de récupérer les valeurs d'une nomenclature (code répertoire) de SIHAM
     *
     * @param $params  array
     *
     * @return array
     */

    public function recupererNomenclatureRH(array $params): array
    {
        $listeNomenclatures = [];

        foreach ($params['listeNomenclatures'] as $nomenclature) {
            $listeNomenclatures[] = ['codeRepertoire' => $nomenclature];
        }

        $paramsWS = ['ParamNomenclature' => [
            'codeAdministration' => (isset($params['codeAdministration'])) ? strtoupper($params['codeAdministration']) : '',
            'dateObservation'    => (isset($params['dateObservation'])) ? $params['dateObservation'] : date("Y-m-d"),
            'listeNomenclatures' => $listeNomenclatures,
        ]];


        try {
            $client = $this->sihamClient->getClient('DossierParametrageWebService');
            $result = $client->RecupNomenclaturesRH($paramsWS);
            $this->sendDebug('WS DossierParametrageWebService / METHOD RecupNomenclaturesRH');

            if (isset($result->return)) {
                $nomenclature = [];
                //traitement pour classer alpha sur le libelle
                foreach ($result->return as $value) {
                    $nomenclature[$value->codeNomenclature] = $value->libLongNomenclature;
                }
                asort($nomenclature);

                return $nomenclature;
            }
        } catch (Exception $e) {
            $this->sendDebug('WS DossierParametrageWebService / METHOD RecupNomenclaturesRH', $e);
            throw new SihamException($e->getMessage(), 0, $e);
        }
    }



    public function recupererListeStatuts($from = '')
    {
        if (!empty($this->sihamConfig['filters'][$this->codeNomenclatureStatuts])) {
            return $this->sihamConfig['filters'][$this->codeNomenclatureStatuts];
        }

        $params = ['codeAdministration' => $this->codeAdministration,
                   'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
                   'listeNomenclatures' => [$this->codeNomenclatureStatuts],];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeGrades($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureGrades],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeModalites($from = '')
    {
        if (!empty($this->sihamConfig['filters'][$this->codeNomenclatureModalites])) {
            return $this->sihamConfig['filters'][$this->codeNomenclatureModalites];
        }

        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => $from,
            'listeNomenclatures' => [$this->codeNomenclatureModalites],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListePositions($from = '')
    {
        if (!empty($this->sihamConfig['filters'][$this->codeNomenclaturePositions])) {
            return $this->sihamConfig['filters'][$this->codeNomenclaturePositions];
        }

        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclaturePositions],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeEmplois()
    {
        if (!empty($this->sihamConfig['filters']['emplois'])) {
            return $this->sihamConfig['filters']['emplois'];
        }

        return [];
    }



    public function recupererListeCorps($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureCorps],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeSectionsCnu($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureSectionsCnu],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeContrats($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureContrats],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeFamillesProfessionnelles($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureFamillesProfessionnelles],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeCategories($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureCategories],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeEchelons($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureEchelons],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeAdminsitrations($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureAdministrations],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeEtablissements($from = '')
    {
        $params = [
            'codeAdministration' => $this->codeAdministration,
            'dateObservation'    => (!empty($from)) ? $from : date('Y-m-d'),
            'listeNomenclatures' => [$this->codeNomenclatureEtablissements],
        ];

        return $this->recupererNomenclatureRH($params);
    }



    public function recupererListeUO(array $params)
    {
        //Traitement des listes unité organisationnelle
        $listeUO = [];
        if (!empty($params['listeUO'])) {
            foreach ($params['listeUO'] as $uo) {
                $listeUO[] = [
                    'codeUO'      => (isset($uo['codeUO'])) ? strtoupper($uo['codeUO']) : '',
                    'structureUO' => (isset($uo['structureUO'])) ? strtoupper($uo['structureUO']) : '',
                    'typeUO'      => (isset($uo['typeUO'])) ? strtoupper($uo['typeUO']) : '',
                ];
            }
        } else {
            $listeUO[] = [
                'codeUO'      => '',
                'structureUO' => '',
                'typeUO'      => '',
            ];
        }


        $paramsWS = ['ParamStructure' => [
            'codeAdministration' => $this->getCodeAdministration(),
            'dateObservation'    => (isset($params['dateObservation'])) ? $params['dateObservation'] : '',
            'listeUO'            => $listeUO,
        ]];


        try {
            $client = $this->sihamClient->getClient('DossierParametrageWebService');
            $result = $client->RecupStructures($paramsWS);
            $this->sendDebug('WS DossierParametrageWebService / METHOD RecupStructures');


            if (isset($result->return)) {
                $unitesOrganisationnelles = [];
                foreach ($result->return as $value) {
                    $unitesOrganisationnelles[$value->codeUO] = $value->codeUO . ' ' . $value->libCourtUO;
                }
                //Supprimer une composante de la liste
                if (!empty($this->sihamConfig['unites-organisationelles']['excludes'])) {
                    $excludes = $this->sihamConfig['unites-organisationelles']['excludes'];
                    foreach ($excludes as $code) {
                        if (array_key_exists($code, $unitesOrganisationnelles)) {
                            unset($unitesOrganisationnelles[$code]);
                        }
                    }
                }
                //Ajouter une composante spécifique
                if (!empty($this->sihamConfig['unites-organisationelles']['includes'])) {
                    $includes = $this->sihamConfig['unites-organisationelles']['includes'];
                    foreach ($includes as $code => $libelle) {
                        if (!array_key_exists($code, $unitesOrganisationnelles)) {
                            $unitesOrganisationnelles[$code] = $libelle;
                        }
                    }
                }
                ksort($unitesOrganisationnelles);
                if (isset($this->sihamConfig['unites-organisationelles']['regex'])) {
                    $unitesOrganisationnelles = str_replace($this->sihamConfig['unites-organisationelles']['regex']['search'],
                        $this->sihamConfig['unites-organisationelles']['regex']['replace'], $unitesOrganisationnelles);
                }

                return $unitesOrganisationnelles;
            }
        } catch (\SoapFault $e) {
            throw new SihamException($e->getMessage(), 0, $e);
        }
    }



    public function formatCoordoonneesBancairesForSiham($iban, $bic)
    {
        //On récupére l'iban et on doit le décompser pour récupérer les informations nécessaire à SIHAM
        $coordonnees               = [];
        $coordonnees['paysBanque'] = (substr($iban, 0, 2) == 'FR') ? 'FRA' : substr($iban, 0, 2);
        $coordonnees['codeBanque'] = substr($iban, 4, 5);
        $coordonnees['codeAgence'] = substr($iban, 9, 5);
        $reste                     = substr($iban, 14);
        $coordonnees['numCompte']  = substr($reste, 0, strlen($reste) - 2);
        $coordonnees['cleCompte']  = substr($iban, strlen($iban) - 2, 2);
        $coordonnees['IBAN']       = $iban;
        $coordonnees['SWIFT']      = $bic;


        return $coordonnees;
    }



    public function getCodeEtablissement()
    {
        return $this->codeEtablissement;
    }



    public function getCodeAdministration()
    {
        return $this->codeAdministration;
    }



    public function sendDebug($message = '', $exception = null): bool
    {
        $config = $this->sihamConfig;
        if ($config['debug']['activate'] === false) {
            return false;
        }


        if ($config['debug']['onlyException'] === true && empty($exception)) {
            return false;
        }

        if (empty($exception)) {
            $e     = new \Exception();
            $trace = $e->getTraceAsString();
        } else {
            $trace = $exception->getMessage();
            $trace .= "----------------------------------------------------";
            $trace .= $exception->getTraceAsString();
        }

        $body = $trace;
        $body .= "----------------------------------------------------";
        $body .= $this->sihamClient->getLastRequest();
        $body .= "----------------------------------------------------";
        $body .= $this->sihamClient->getLastResponse();

        $mail = new Email();
        $mail->html($body);
        $mail->from(new Address($config['debug']['from'], 'Application OSE'));
        $mail->to($config['debug']['to']);

        if (!empty($exception)) {
            $mail->subject('Exception Siham' . ' / ' . $message);
        } else {
            $mail->subject('Debug Siham' . ' / ' . $message);
        }

        $transport = new EsmtpTransport(host: $config['debug']['smtpHost'], port:$config['debug']['smtpPort']);
        $mailer = new Mailer($transport);

        try{
            $mailer->send($mail);
            return true;
        }
        catch(TransportExceptionInterface $e)
        {
            return false;
        }
    }
}