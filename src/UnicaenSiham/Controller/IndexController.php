<?php

namespace UnicaenSiham\Controller;


use UnicaenSiham\Service\Traits\SihamAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;


class IndexController extends AbstractActionController
{
    use SihamAwareTrait;

    public function indexAction(): array
    {
        return [];
    }

}
