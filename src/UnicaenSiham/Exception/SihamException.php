<?php

namespace UnicaenSiham\Exception;


class SihamException extends \Exception
{

    protected $errorMessages = [
        "PARAMETRES_VIDES"                       => "Aucun paramétre n'a été passé à l'API SIHAM",
        "is not a valid method for this service" => "La méthode appelée n'est pas disponible via l'API SIHAM",
        "MATRICULE_VIDE"                         => "Aucun paramétre matricule valide n'a été fourni à l'API SIHAM",
        "ERREUR_NATVOI"                          => "La nature de la voie de l'adresse est invalide",
        "DATDEB_ADRESSE_NON_TROUVE"              => "La date de début de l'adresse n'a pas été fourni à l'API",
        "TYPADD_DEJA_PRESENT"                    => "Le type d'adresse pour cette agent existe déjà",
        "ADR_VIDE"                               => "Aucune adresse n'a été renseigné",
        "AJOUT_IMPOSSIBLE"                       => "Ajout de l'adresse impossible",
        "TYPTEL_DEJA_PRESENT"                    => "Un type de numéro de téléphone existe déjà pour cet agent",
        "LONG_COMPL_ADRESSE_SUPERIEURE_A_38 "    => "La longeur de l'adresse dépasse 38 caractéres",
        "failed to load external entity"         => "Les webservices SIHAM ne sont pas disponibles pour le moment.",
    ];


    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        $translateMessage = '';
        $defaultMessage = "Un problème est survenu lors de l'appel à l'API SIHAM / " . $message;

        if (!is_null($previous) && $previous->getMessage() !== $message) {
            $defaultMessage .= "(" . $previous->getMessage() . ")";
        }


        foreach ($this->errorMessages as $error => $mess) {

            if (preg_match("/$error/", $message)) {
                $translateMessage = "Un problème est survenu lors de l'appel à l'API SIHAM / " . $mess;

                if ($error == 'failed to load external entity') {
                    $translateMessage = $mess;
                }
                break;
            }
        }

        $message = (!empty($translateMessage)) ? $translateMessage : $defaultMessage;

        return parent::__construct($message, $code, $previous);
    }


    public function __toString()
    {
        return $this->getMessage();
    }

}