# UnicaenSiham

## Introduction

Il s'agit d'une bibliothèque UNICAEN, permettant de connecter une application avec le SI RH Siham :

* [Rechercher un agent](./doc/RechercherAgent.md)
* [Récupérer une liste d'agent](./doc/RecupererListeAgent.md)
* [Récupérer les données personnelles d'un agent](./doc/RecupererDonneesPersonnellesAgent.md)
* Modifier les coordonnées bancaires d'un agent
* [Modifier l'adresse principale d'un agent](./doc/ModifierAdressePrincipaleAgent.md)
* [Modifier coordonnées (téléphones et emails) d'un agent](./doc/ModifierCoordonneesAgent.md)
* [Historiser les coordonnées (téléphones et emails) d'un agent](./doc/HistoriserCoordonneesAgent.md)
* Prendre en charge un agent
* Renouveller un agent
* [Récupérer les nomenclatures (types de statut, types de contrat, liste des modalités de service etc...)](./doc/RecupererNomenclatures.md)
* Récupérer la liste des unités organisationnelles (UO)

## Configuration

Si vous utilisez le module siham unicaen, vous devez renseigner un certain nombre de paramètres nécessaires au bon fonctionnement de celui-ci.

Vous pouvez récupérer un modèle de fichier de configuration dans config/unicaen-siham.global.php.dist.

Dans ce fichier de configuration il faudra préciser :

* Les identifiant et mot de passe des APIs Siham
* Les différentes URL des APIs Siham
* La version du client SOAP utilisée
* Le code permettant de remonter les structures d'affectation
* Le code administration de SIHAM
* Le code établissement de SIHAM
* Activer ou non les filtres sur les nomenclatures.
* Activer ou non la création du contrat lors de la PEC ou Renouveller
* Paramètrage pour la création du contrat dans SIHAM
* Choisir le type d'affectation remonté

## Exemples de requêtes SOAP

Vous trouverez également dans le repertoire [datas](./datas/) des exemples type de requêtes SOAP pour les APIs Siham.

## Postman

Si vous utilisez le logiciel Postman ([https://www.postman.com](https://www.postman.com/)) pour découvrir les APIs, vous pouvez importer le fichier de définition de l'ensemble des APIs : [siham.json](./siham.json). Il faudra juste modifier le hostname de vos APIs et les crédentials.

